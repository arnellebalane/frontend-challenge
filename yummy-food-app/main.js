const carousel = (function() {
    const element = document.querySelector('.carousel');
    const items = Array.from(element.querySelectorAll('.carousel__image'));
    const itemWidth = 200;
    const currentItemClass = 'carousel__image--current';
    let index = 0;

    function render(index) {
        items.forEach((item, i) => {
            const transforms = [`translateX(-${itemWidth * index}px)`];
            if (item.classList.contains(currentItemClass)) {
                item.classList.remove(currentItemClass);
            }
            if (index === i) {
                item.classList.add(currentItemClass);
            } else {
                transforms.push('scale(0.8)');
            }
            item.style.transform = transforms.join(' ');
        });
    }

    function previous() {
        if (index > 0) {
            render(--index);
        }
        return index;
    }

    function next() {
        if (index < items.length - 1) {
            render(++index);
        }
        return index;
    }

    return { render, previous, next };
})();


const data = [{
    color: '#faaaa1',
    name: 'Drinks in the sun',
    description: 'Hot days for frosty sips! Try this refreshing drink that will impress all your friends.'
}, {
    color: '#f8cf8b',
    name: 'Fresh Veggies',
    description: 'Crunchy and savory, best served with a glass of something cold in good company'
}, {
    color: '#bbd686',
    name: 'Drinks in the sun',
    description: 'Hot days for frosty sips! Try this refreshing drink that will impress all your friends.'
}, {
    color: '#788585',
    name: 'Fresh Veggies',
    description: 'Crunchy and savory, best served with a glass of something cold in good company'
}, {
    color: '#508aa8',
    name: 'Drinks in the sun',
    description: 'Hot days for frosty sips! Try this refreshing drink that will impress all your friends.'
}, {
    color: '#f8cf8b',
    name: 'Fresh Veggies',
    description: 'Crunchy and savory, best served with a glass of something cold in good company'
}, {
    color: '#faaaa1',
    name: 'Drinks in the sun',
    description: 'Hot days for frosty sips! Try this refreshing drink that will impress all your friends.'
}];



document.querySelector('.carousel').addEventListener('click', function(e) {
    const target = e.target;
    const currentClass = 'carousel__image--current';
    if (target.classList.contains('carousel__image')
    && !target.classList.contains(currentClass)) {
        const previous = target.previousElementSibling;
        const next = target.nextElementSibling;
        if (previous && previous.classList.contains(currentClass)) {
            var index = carousel.next();
        } else if (next && next.classList.contains(currentClass)) {
            var index = carousel.previous();
        }

        const item = data[index];
        document.querySelector('.screen').style.setProperty('--primary-color', item.color);

        document.querySelector('.recipe__title').textContent = item.name;
        document.querySelector('.recipe__description').textContent = item.description;
    }
});
